//
//  AppDelegate.swift
//  Buddy
//
//  Created by Yash salgaonkar on 11/02/23.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //Code to run every Monday
        let today = Date()
        let calendar = Calendar.current
        let weekday = calendar.component(.weekday, from: today)
        if weekday == 4 {
            
            //call function to look through database
            
            var weeklyEvent = createWeeklyEvent()
            
            //To be replaced by weeklyEvent title and description
            let content = UNMutableNotificationContent()
            content.title = weeklyEvent.title
            content.body = weeklyEvent.description
            content.sound = .default
            
            let request = UNNotificationRequest(identifier: "MondayNotification", content: content, trigger: nil)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            
            completionHandler(.newData)
        } else {
            completionHandler(.noData)
        }
    }
    
    
    //this looks through the database to suggest Events to users, returns an Event to be sent as a notification
    
    func createWeeklyEvent() -> Event {
        
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        var weeklyEvent: Event? = nil
        var success = 0
        //var eventId = 0
        //tables
        //EventInvitees: event_id(TEXT), user_id(TEXT), response(TEXT) : 0=Neutral, 1=Accept, 2=Decline
        //EventTags: event_id, tag_id
        //UserInterests: user_id, interest_id
        
        if database.open(){
            
            var userCount = 0
            var eventCount = 0
            
            //Counting users
            let userQuery = "SELECT COUNT(*) FROM Users"
            if let resultSet = database.executeQuery(userQuery, withArgumentsIn: []) {
                if resultSet.next() {
                    userCount = Int(resultSet.int(forColumnIndex: 0))
                }
                resultSet.close()
            } else {
                print("Couldn't fetch count")
            }
            
            //Counting events
            let eventQuery = "SELECT COUNT(*) FROM Events"
            if let resultSet = database.executeQuery(eventQuery, withArgumentsIn: []) {
                if resultSet.next() {
                    eventCount = Int(resultSet.int(forColumnIndex: 0))
                }
                resultSet.close()
            } else {
                print("Couldn't fetch count")
            }
            
            //No need for two approaches just compare tags (previously interests - now merged for ease)
            
            //1]Firstly check if our user's already been invited to another Event of type = "system" with today's date. If not, proceed -> userAvailable()?
            //Device user's username
            if let userDict = UserDefaults.standard.dictionary(forKey: "loggedInUser") {
                let username = userDict["username"] as! String
                if userAvailable(userId: username){
                    //Step 2
                    //2.1 Conversion to [1,0,...]
                    let tagsArray = getUserTagsBinaryArray(username: username)
                    //2.2 Comparison
                    var mostSimilarUsers = topSimilarUsers(username: username, tagsArray: tagsArray)
                    mostSimilarUsers.append(username) //Adding current user as well, as system is the creator
                    //3 If mostSimilarUsers is empty
                    //...
                    //4 and 5: Creating event
                    let creator_id = "-1" // -1 for system
                    let currentDate = Date()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd/MM/yyyy"
                    let dateString = dateFormatter.string(from: currentDate)
                    let title = "Buddy-Up " + dateString
                    let description = "HI! We think that you all share similar interests and should definitely hang out"
                    let event_type = "system"
                    let date = "TBD"
                    let time = "TBD"
                    let address = "TBD"
                    
                    
                    if date != "" && time != "" {
                        database.open()
                        let query = "INSERT INTO Events (location, date, time, creator_id, title, description, event_type) VALUES (?, ?, ?, ?, ?, ?, ?)"
                        do {
                            try database.executeUpdate(query, values: [address, date, time, creator_id ?? "", title, description, event_type])
                            
                            let eventId = database.lastInsertRowId
                            
                            
                            //Now inserting into EventInvitees
                            for similarUser in mostSimilarUsers {
                                
                                let query = "INSERT INTO EventInvitees (event_id, user_id, response) VALUES (?, ?, ?)"
                                do {
                                    try database.executeUpdate(query, values: [eventId, similarUser, "0"])
                                } catch {
                                    print("Couldn't insert into EventInvitees table: \(error.localizedDescription)")
                                }
                            }
                            
                            //Event doesn't really need tags as it's not accepted or declined by anyone
                            success = 1

                            //returning created event
                            let query = "SELECT * FROM Events WHERE id = ?"
                            if let resultSet = database.executeQuery(query, withArgumentsIn: [eventId]) {
                                if resultSet.next() {
                                    weeklyEvent = Event(id: resultSet.string(forColumn: "event_id")!,location: resultSet.string(forColumn: "location")!,date: resultSet.string(forColumn: "date")!, time: resultSet.string(forColumn: "time")!,creator_id: resultSet.string(forColumn: "creator_id")!, title: resultSet.string(forColumn: "title")!, description: resultSet.string(forColumn: "description")!)
                                }
                                resultSet.close()
                            } else {
                                print("Couldn't fetch the last stored event")
                            }
                            
                        } catch {
                            print("Error inserting data into Events table: \(error.localizedDescription)")
                        }
                        
                        
                        
                        database.close()
                    }
                }
                else{
                    //Do nothing, return event with today's title
                    let creator_id = "-1"
                    let currentDate = Date()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd/MM/yyyy"
                    let dateString = dateFormatter.string(from: currentDate)
                    let title = "Buddy-Up " + dateString
                    
                    var newestEventId = ""
                    var userEventId = ""
                    
                    database.open()
                    
                    
                    
                    // Get the event's id
                    let query = "SELECT * FROM Events WHERE title = ?"
                    if let oResultSet = database.executeQuery(query, withArgumentsIn: [title]) {
                        if oResultSet.next() {
                            newestEventId = oResultSet.string(forColumn: "id")!
                            let inviteeQuery = "SELECT * FROM EventInvitees WHERE user_id = ? AND event_id = ?"
                            if let iResultSet = database.executeQuery(inviteeQuery, withArgumentsIn: [username, newestEventId]) {
                                if iResultSet.next() {
                                    weeklyEvent = Event(id: oResultSet.string(forColumn: "id")! ,location: oResultSet.string(forColumn: "location") ?? "NA" ,date: oResultSet.string(forColumn: "date")  ?? "NA" , time: oResultSet.string(forColumn: "time")  ?? "NA" ,creator_id: oResultSet.string(forColumn: "creator_id")  ?? "-1" , title: oResultSet.string(forColumn: "title")!, description: oResultSet.string(forColumn: "description")  ?? "" )
                                }
                                iResultSet.close()
                            } else {
                                print("Couldn't fetch the last stored event")
                            }
                        }
                        oResultSet.close()
                    } else {
                        print("Couldn't fetch the last stored event")
                    }
                    
                    
                    database.close()
                    
                }

                    
                
                
                
                
            }
            
            
            
            
            
            //2]Convert the user's interest array in the type [1,0,...] and calculate it's cosine similarity (use cosineSimilarity) with the same array for every other user (WHO IS AVAILABLE -> userAvailable()?)
            
            //3]If no other users available i.e. if list is empty or has less than 2 then
            //1) go through list of Events that were created today
            //2) compare the cosineSimilarity of all events' tags with the user's interest array (similar to user-user)
            //3) Append the user to the most similar Event i.e add to EventInvitees
            //4) Use the id to fetch the remaining details from Events
            //5) Return this existing Event
            
            //4]Take the top 3 users from this returned list
            //5]Create an event and add all 4 users (including current) to the Event
            //6]Name it after the current date for now
            //7]return this event
            
            
            //Send a push notification fot the event -> in the func application ()
            
            
        }
        
        return weeklyEvent!
    }
    
    func getUserTagsBinaryArray(username: String) -> [Int] {
        var tagsArray = [Int]()
        
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        if database.open(){
            
            //Total number of tags
            let tagsQuery = "SELECT COUNT(*) as count FROM Tags"
            if let tagsResult = database.executeQuery(tagsQuery, withArgumentsIn: []) {
                if tagsResult.next() {
                    let totalTags = Int(tagsResult.int(forColumn: "count"))
                    
                    //Filling with 0's
                    tagsArray = [Int](repeating: 0, count: totalTags)
                    
                    //Getting tags
                    let userTagsQuery = "SELECT tag_id FROM UserTags WHERE username = ?"
                    if let userTagsResult = database.executeQuery(userTagsQuery, withArgumentsIn: [username]) {
                        while userTagsResult.next() {
                            let tagId = Int(userTagsResult.int(forColumn: "tag_id")) - 1
                            if tagId < tagsArray.count {
                                //Setting corresponding element to 1
                                tagsArray[tagId] = 1
                            }
                        }
                    }
                }
            }
            database.close()
        }
        else{
            print("Unable to open database in order to get binary tags array")
        }
        
        return tagsArray
    }
    
    //returns 3 most similar users
    func topSimilarUsers(username: String, tagsArray: [Int]) -> [String] {
        var usernameArray = [String]()
        var similarityArray = [Double]()
        var similarUsers = [String]()
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        if database.open(){
            
            let getUsersQuery = "SELECT username FROM User WHERE username != ?"
            guard let getUsersResult = database.executeQuery(getUsersQuery, withArgumentsIn: [username]) else {
                print("Unable to execute query for getting users")
                return similarUsers
            }
            while getUsersResult.next() {
                let otherUsername = getUsersResult.string(forColumn: "username") ?? ""
                if userAvailable(userId: otherUsername) {
                    let otherUserTagsBinaryArray = getUserTagsBinaryArray(username: otherUsername)
                    let similarity = calculateCosineSimilarity(array1: tagsArray, array2: otherUserTagsBinaryArray)
                    similarityArray.append(similarity)
                    usernameArray.append(otherUsername)
                }
            }
            getUsersResult.close()
            
            let sortedArray = zip(usernameArray, similarityArray).sorted(by: {$0.1 > $1.1})
            //Appending 3 most similar users
            for i in 0..<3 {
                similarUsers.append(sortedArray[i].0)
            }
            
            
            database.close()
        }
        else{
            print("ERROR: Unable to open database for meta information")
        }
        
        return similarUsers
    }
    
    func calculateCosineSimilarity(array1: [Int], array2: [Int]) -> Double {
        var dotProduct = 0.0
        var normA = 0.0
        var normB = 0.0
        var similarity = 0.0

        for i in 0..<array1.count {
            dotProduct += Double(array1[i] * array2[i])
            normA += Double(array1[i] * array2[i])
            normB += Double(array2[i] * array2[i])
        }

        if (sqrt(normA) * sqrt(normB)) != 0 {
            similarity = dotProduct / (sqrt(normA) * sqrt(normB))
        }
        else {
            similarity = 0.0
        }
        
        return similarity
    }

    
    
    
    func userAvailable(userId: String) -> Bool {
        let today = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let todayString = dateFormatter.string(from: today)
        
        var userAvailable = true
        
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        if database.open(){
            // Get today's event's id, if exists
            let eventsQuery = "SELECT id FROM Events WHERE title LIKE ? AND event_type = 'system'"
            if let eventsResult = database.executeQuery(eventsQuery, withArgumentsIn: ["%\(todayString)%"]) {
                while eventsResult.next() {
                    let eventId = eventsResult.string(forColumn: "id")
                    
                    // Check if user has already been invited to this event
                    let inviteesQuery = "SELECT * FROM EventInvitees WHERE event_id = ? AND user_id = ?"
                    if let inviteesResult = database.executeQuery(inviteesQuery, withArgumentsIn: [eventId, userId]) {
                        if inviteesResult.next() {
                            
                            userAvailable = false
                            break
                            
                        }
                    }
                }
            }
            
            database.close()
        }
        else {
            print("Unable to open database to check user availability")
        }
        
        
        return userAvailable
    }
        
        
        
        func testCode() {
            /*
             let user1 = User(firstName: "John", lastName: "Doe", email: "jd", interests: ["Football","Rowing"])
             let user2 = User(firstName: "Jane", lastName: "Doe", email: "jd", interests: ["Tennis","Bowling"])
             
             let event = Event(name: "Party", host: user1)
             event.sendInvitation(to: user2)
             
             let invite = Invite(sender: user1, recipient: user2, event: event)
             
             user2.receivedInvites.append(invite)
             
             user2.acceptInvite(invite)
             
             print("Event name: \(event.name)")
             print("Event host: \(event.host.firstName)")
             print("Event guests: \(event.guests.map { $0.firstName })")
             print("Invite sender: \(invite.sender.firstName)")
             print("Invite recipient: \(invite.recipient.firstName)")
             print("Invite status: \(invite.status)")
             */
        }
        
        func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
            // Override point for customization after application launch.
            testCode()
            return true
        }
        
        // MARK: UISceneSession Lifecycle
        
        func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
            // Called when a new scene session is being created.
            // Use this method to select a configuration to create the new scene with.
            return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
        }
        
        func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
            // Called when the user discards a scene session.
            // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
            // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
        }
        
        
        
    
    
}
