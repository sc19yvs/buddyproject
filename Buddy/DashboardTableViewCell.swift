//
//  DashboardTableViewCell.swift
//  Buddy
//
//  Created by Yash salgaonkar on 13/04/23.
//

import UIKit

protocol DashboardTableViewCellDelegate: AnyObject {
    func didTapTabButton(buttonTitle: String)
}

class DashboardTableViewCell: UITableViewCell {

    weak var delegate: DashboardTableViewCellDelegate?
    
    @IBOutlet var tabButton: UIButton!
    
    
    @IBAction func tabButtonTapped(_ sender: Any) {
        delegate?.didTapTabButton(buttonTitle: tabButton.titleLabel?.text ?? "")
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tabButton.addTarget(self, action: #selector(tabButtonTapped), for: .touchUpInside)
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
