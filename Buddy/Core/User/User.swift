//
//  User.swift
//  Buddy
//
//  Created by Yash salgaonkar on 11/02/23.
//

import Foundation

class User: Hashable {
    
    
    var first_name: String
    var last_name: String
    var username: String
    var password: String
    var date_of_birth: String
    var description: String
    var course: String
    var profile_picture: Data
    var buddy_enabled: String
    var location_enabled: String

    init(first_name: String, last_name: String, username: String, password: String, date_of_birth: String, description: String, course: String, buddy_enabled: String, location_enabled: String, profilePicture: Data) {
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.password = password
        self.date_of_birth = date_of_birth
        self.description = description
        self.course = course
        self.profile_picture = profilePicture
        self.buddy_enabled = buddy_enabled
        self.location_enabled = location_enabled
        
        
    }
    
    static func == (lhs: User, rhs: User) -> Bool {
            return lhs.username == rhs.username
        }

        func hash(into hasher: inout Hasher) {
            hasher.combine(username)
        }
    
    //YS: need to check if guest is already on list, don't invite twice
   
}

func getUser(username: String, password: String, database: FMDatabase) -> User? {
    var user: User?
        
    do {
        try database.open()
        
        let query = "SELECT * FROM User WHERE username = ? AND password = ?"
        let results = try database.executeQuery(query, values: [username, password])
        
        if results.next() {
            let first_name = results.string(forColumn: "first_name") ?? ""
            let last_name = results.string(forColumn: "last_name") ?? ""
            //let email = results.string(forColumn: "email") ?? ""
            let username = results.string(forColumn: "username") ?? ""
            let password = results.string(forColumn: "password") ?? ""
            let date_of_birth = results.string(forColumn: "date_of_birth") ?? ""
            let description = results.string(forColumn: "description") ?? ""
            let course  = results.string(forColumn: "course") ?? ""
            let buddy_enabled = results.string(forColumn: "buddy_enabled") ?? "no"
            let location_enabled = results.string(forColumn: "location_enabled") ?? "no"
            let profilePicture = results.data(forColumn: "profile_picture") ?? Data()
            user = User(first_name: first_name, last_name: last_name, username: username, password: password, date_of_birth: date_of_birth, description: description, course: course, buddy_enabled: buddy_enabled, location_enabled: location_enabled, profilePicture: profilePicture ?? Data())
            
            current_user = user //saving current user
        }
        
        database.close()
    } catch {
        print("Error: \(error.localizedDescription)")
    }
        
    return user





}
