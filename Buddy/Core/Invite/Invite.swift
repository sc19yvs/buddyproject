//
//  Invite.swift
//  Buddy
//
//  Created by Yash salgaonkar on 11/02/23.
//

import Foundation

class Invite {
    let sender: User
    let recipient: User
    let event: Event
    var status: InviteStatus

    enum InviteStatus {
        case pending
        case accepted
        case declined
    }

    init(sender: User, recipient: User, event: Event) {
        self.sender = sender
        self.recipient = recipient
        self.event = event
        self.status = .pending
    }
    
    
}

extension Invite: Equatable {
    static func == (lhs: Invite, rhs: Invite) -> Bool {
        return lhs.sender == rhs.sender &&
               lhs.recipient == rhs.recipient &&
               lhs.event == rhs.event
    }
}
