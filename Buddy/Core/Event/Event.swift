//
//  Event.swift
//  Buddy
//
//  Created by Yash salgaonkar on 11/02/23.
//

import Foundation

class Event: Hashable {
    var id: String
    var location: String
    var date: String
    var time: String
    var creator_id: String
    var title: String
    var description: String

    init(id: String, location: String, date: String, time: String, creator_id: String, title: String, description: String) {
        self.id = id
        self.location = location
        self.date = date
        self.time = time
        self.creator_id = creator_id
        self.title = title
        self.description = description
    }

    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
      }
}

extension Event: Equatable {
    static func ==(lhs: Event, rhs: Event) -> Bool {
        return lhs.id == rhs.id
    }
}
