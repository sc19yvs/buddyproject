//
//  GroupInfoViewController.swift
//  Buddy
//
//  Created by Yash salgaonkar on 03/04/23.
//

import UIKit

class GroupInfoViewController: UIViewController,  UITableViewDelegate, UITableViewDataSource, UserTableViewCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as! UserTableViewCell
        let user = users[indexPath.row]
        cell.nameLabel.text = user.first_name
        cell.profileImageView.image = UIImage(data: user.profile_picture)
        cell.profileImageView.contentMode = .scaleAspectFit

        return cell
    }
    

    var groupName: String = ""
    var dateTime: String = ""
    var desc: String = ""
    var address: String = ""
    var event_id: String = ""
    var users: [User] = []
    
    @IBOutlet var groupNameTextView: UITextView!
    
    
    @IBOutlet var dateTimeLabel: UILabel!
    
    @IBOutlet var addressTextView: UITextView!
    
    @IBOutlet var descriptionTextView: UITextView!
    
    @IBOutlet var usersTableView: UITableView!
    
    @IBOutlet var edit: UIButton!
    
    @IBAction func editTapped(_ sender: Any) {
        if edit.currentTitle == "Edit" {
            edit.setTitle("Done", for: .normal)
            dateTimeLabel.isHidden = true
            dateTimePicker.isHidden = false
            groupNameTextView.isEditable = true
            addressTextView.isEditable = true
            descriptionTextView.isEditable = true
        }
        else {
            edit.setTitle("Edit", for: .normal)
            dateTimeLabel.isHidden = false
            dateTimePicker.isHidden = true
            groupNameTextView.isEditable = false
            addressTextView.isEditable = false
            descriptionTextView.isEditable = false
        }
    }
    
    
    @IBOutlet var dateTimePicker: UIDatePicker!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Hiding
        dateTimePicker.isHidden = true
        
        //Setting labels
        dateTimeLabel.text = "When: " + dateTime
        addressTextView.text = "Where: " + address
        //addressLabel.text = address
        groupNameTextView.text = groupName
        descriptionTextView.text = desc
        
        usersTableView.dataSource = self
        usersTableView.delegate = self
        
        let nib = UINib(nibName: "UserTableViewCell", bundle: nil)
        
        usersTableView.register(nib, forCellReuseIdentifier: "UserTableViewCell")
        
        //Load usernames into an array
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        if !database.open(){
            print("Unable to open DB")
        }
        else{
            let outerQuery = "SELECT * FROM EventInvitees WHERE event_id = ? "
            if let oResults = database.executeQuery(outerQuery, withArgumentsIn: [event_id]) {
                while oResults.next(){
                    let username = oResults.string(forColumn: "user_id") ?? ""
                    let innerQuery = "SELECT * FROM User WHERE username = ? "
                    print("event ID: \(event_id)")
                    if let results = database.executeQuery(innerQuery, withArgumentsIn: [username]) {
                        while results.next() {
                            if let resultUsername = results.string(forColumn: "username"),
                               let resultPassword = results.string(forColumn: "password"),
                               let resultFirstName = results.string(forColumn: "first_name"),
                               let resultLastName = results.string(forColumn: "last_name"),
                               let resultDOB = results.string(forColumn: "date_of_birth"),
                               let resultDescription = results.string(forColumn: "description"),
                               let resultCourse = results.string(forColumn: "course"),
                               let buddy_enabled = results.string(forColumn: "buddy_enabled"),
                               let location_enabled = results.string(forColumn: "location_enabled"){
                                let user = User(first_name: resultFirstName, last_name: resultLastName, username: resultUsername, password: resultPassword, date_of_birth: resultDOB, description: resultDescription, course: resultCourse, buddy_enabled: buddy_enabled, location_enabled: location_enabled, profilePicture: results.data(forColumn: "profile_picture") ?? Data())
                                users.append(user)
                            }
                        }
                    } else {
                        print("Error: results is nil")
                    }
                }
            }
            database.close()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 73
    }
    


}
