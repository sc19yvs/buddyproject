//
//  CreateViewController.swift
//  Buddy
//
//  Created by Yash salgaonkar on 17/02/23.
//

import UIKit

class CreateViewController: UIViewController, NextCreateViewControllerDelegate {

    //list of selected users
    var selectedUsers: [User] = []
    var selectedTags: [String] = []
    var isOpen = false
    
    @IBOutlet var openEventButton: UIButton!
    
    @IBOutlet var inviteFriends: UIButton!
    
    //TABS
    /*
    1   Coffee
    2   Pub
    3   Socialise
    4   Library
    5   Football
    6   Club
    7   Food
    8   Festival
    9   Marathon
    10  Exercise
    11  Gym
    12  Coding
    13  Shopping
    14  Day trip
    15  Study
    16  Exploring the city
    17  Coursemates
    18  Gaming
    */
    @IBAction func coffeeTapped(_ sender: UIButton) {
        
        if selectedTags.contains("1") {
            if let index = selectedTags.firstIndex(of: "1") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("1")
            sender.backgroundColor = UIColor.systemOrange
        }
         
    }
    
    @IBAction func pubTapped(_ sender: UIButton) {
        if selectedTags.contains("2") {
            if let index = selectedTags.firstIndex(of: "2") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("2")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func socialiseTagged(_ sender: UIButton) {
        if selectedTags.contains("3") {
            if let index = selectedTags.firstIndex(of: "3") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("3")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func gymTapped(_ sender: UIButton) {
        if selectedTags.contains("11") {
            if let index = selectedTags.firstIndex(of: "11") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("11")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func exploringTapped(_ sender: UIButton) {
        if selectedTags.contains("16") {
            if let index = selectedTags.firstIndex(of: "16") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("16")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func libraryTapped(_ sender: UIButton) {
        if selectedTags.contains("4") {
            if let index = selectedTags.firstIndex(of: "4") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("4")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func codingTapped(_ sender: UIButton) {
        if selectedTags.contains("12") {
            if let index = selectedTags.firstIndex(of: "12") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("12")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func foodTapped(_ sender: UIButton) {
        if selectedTags.contains("7") {
            if let index = selectedTags.firstIndex(of: "7") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("7")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func festivalTapped(_ sender: UIButton) {
        if selectedTags.contains("8") {
            if let index = selectedTags.firstIndex(of: "8") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("8")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func marathonTapped(_ sender: UIButton) {
        if selectedTags.contains("9") {
            if let index = selectedTags.firstIndex(of: "9") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("9")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func exerciseTapped(_ sender: UIButton) {
        if selectedTags.contains("10") {
            if let index = selectedTags.firstIndex(of: "10") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("10")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func shoppingTapped(_ sender: UIButton) {
        if selectedTags.contains("13") {
            if let index = selectedTags.firstIndex(of: "13") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("13")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func clubTapped(_ sender: UIButton) {
        if selectedTags.contains("6") {
            if let index = selectedTags.firstIndex(of: "6") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("6")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func dayTripTapped(_ sender: UIButton) {
        if selectedTags.contains("14") {
            if let index = selectedTags.firstIndex(of: "14") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("14")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func studyTapped(_ sender: UIButton) {
        if selectedTags.contains("15") {
            if let index = selectedTags.firstIndex(of: "15") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("15")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func coursematesTapped(_ sender: UIButton) {
        if selectedTags.contains("17") {
            if let index = selectedTags.firstIndex(of: "17") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("17")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func gamingTapped(_ sender: UIButton) {
        if selectedTags.contains("18") {
            if let index = selectedTags.firstIndex(of: "18") {
                selectedTags.remove(at: index)
            }
            sender.backgroundColor = UIColor.systemGray3
        } else {
            selectedTags.append("18")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    
    @IBAction func inviteFriendsTapped(_ sender: Any) {
        performSegue(withIdentifier: "selectUsers", sender: self)
    }
    
    // Event is an open event
    @IBAction func openEventTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        isOpen = sender.isSelected
        inviteFriends.isEnabled = !isOpen
    }
    
    
    @IBOutlet var titleText: UITextField!
    
    @IBOutlet var descriptionText: UITextView!
    
    @IBOutlet var dateTimePicker: UIDatePicker!
    
    //will have to be changed to actual location after implementing MVP
    @IBOutlet var locationText: UITextView!
    
    @IBAction func createEventTapped(_ sender: Any) {
        //print(selectedUsers.count)
        //print(selectedTags)
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        
        let title = titleText.text ?? ""
        let description = descriptionText.text ?? ""
        //getting date and time
        let selectedDate = dateTimePicker.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .full
        dateFormatter.timeStyle = .short
        
        
        let dateString = dateFormatter.string(from: selectedDate)
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day], from: selectedDate)
        let timeComponents = Calendar.current.dateComponents([.hour, .minute], from: selectedDate)

        let year = dateComponents.year
        let month = dateComponents.month
        let day = dateComponents.day
        let hour = timeComponents.hour
        let minute = timeComponents.minute

        let date = "\(day!)/\(month!)/\(year!)"
        let time = "\(hour!):\(minute!)"
        
        
        
        let address = locationText.text ?? ""
        let invitees = ""
        let attendees = ""
        let creator_id = current_user?.username //current user's id
        var event_type = "closed"
        if isOpen {
            event_type = "open"
        }

        //Entering in Events and EventInvitees
        //print(date)
        //print(time)
        
        if date != "" && time != "" {
            database.open()
            let query = "INSERT INTO Events (location, date, time, creator_id, title, description, event_type) VALUES (?, ?, ?, ?, ?, ?, ?)"
            do {
                try database.executeUpdate(query, values: [address, date, time, creator_id ?? "", title, description, event_type])
                
                let eventId = database.lastInsertRowId
                
                
                //Now inserting into EventInvitees
                if !isOpen{
                    for user in selectedUsers {
                        
                        let query = "INSERT INTO EventInvitees (event_id, user_id, response) VALUES (?, ?, ?)"
                        do {
                            try database.executeUpdate(query, values: [eventId, user.username, "0"])
                        } catch {
                            print("Couldn't insert into EventInvitees table: \(error.localizedDescription)")
                        }
                    }
                }
                //Insert into EventTags
                for tag in selectedTags {
                    let query = "INSERT INTO EventTags (event_id, tag_id) VALUES (?, ?)"
                    do {
                        try database.executeUpdate(query, values: [eventId, tag])
                    } catch {
                        print("Can't insert into EventTags table: \(error.localizedDescription)")
                    }
                }
                
            } catch {
                print("Error inserting data into Events table: \(error.localizedDescription)")
            }
            database.close()
        }
        
        //Going back to home screen
        dismiss(animated: true, completion: nil)
        
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectUsers" {
            let nextVC = segue.destination as! NextCreateViewController
            nextVC.selectedUsers = selectedUsers
            nextVC.delegate = self
        }
    }
    
    func nextCreateViewControllerDidSelectUsers(selectedUsers: [User]) {
            self.selectedUsers = selectedUsers
        }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let nextCreateVC = storyboard?.instantiateViewController(withIdentifier: "NextCreateViewController") as? NextCreateViewController {
                nextCreateVC.delegate = self
                print("Delegate of NextCreateViewController is set to: \(nextCreateVC.delegate)")
            }
        
        openEventButton.setImage(UIImage(systemName: "square"), for: .normal)
            openEventButton.setImage(UIImage(systemName: "checkmark.square.fill"), for: .selected)

        openEventButton.addTarget(self, action: #selector(openEventTapped(_:)), for: .touchUpInside)
    }
    
    



}

protocol NextCreateViewControllerDelegate: AnyObject {
    func nextCreateViewControllerDidSelectUsers(selectedUsers: [User])
}
