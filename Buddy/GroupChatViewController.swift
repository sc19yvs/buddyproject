//
//  GroupChatViewController.swift
//  Buddy
//
//  Created by Yash salgaonkar on 16/03/23.
//

import UIKit
import MessageKit
import InputBarAccessoryView

struct Sender: SenderType {
    var photoURL: String
    var senderId: String
    var displayName: String
    
    
}

struct Message: MessageType {
    var sender: MessageKit.SenderType
    var messageId: String
    var sentDate: Date
    var kind: MessageKit.MessageKind
    
    
}

struct Media: MediaItem {
    var url: URL?
    var image: UIImage?
    var placeholderImage: UIImage
    var size: CGSize
    
    
}

class GroupChatViewController: MessagesViewController, MessagesDataSource, MessagesLayoutDelegate, MessagesDisplayDelegate, InputBarAccessoryViewDelegate {
    
    private var messages = [Message]()
    var event_id: String = ""
    var event_title: String = ""
    
    let currentChatter = Sender(photoURL: "", senderId: current_user!.username, displayName: current_user!.first_name)
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageInputBar.delegate = self
        messageInputBar.sendButton.title = "Send"
        messageInputBar.sendButton.tintColor = .systemBlue
        
        
        self.navigationItem.title = event_title
        
        //Back
        let backButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(backButtonTapped))
        navigationItem.leftBarButtonItem = backButton
        
        // Add tap gesture recognizer to navigation item title
        let titleLabel = UILabel()
        titleLabel.text = event_title
        titleLabel.isUserInteractionEnabled = true
        navigationItem.titleView = titleLabel
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(navigationItemTitleTapped))
        navigationItem.titleView?.addGestureRecognizer(tapGesture)
        
        //Adding messages
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        if database.open() {
            let query = "SELECT * FROM Messages WHERE event_id = ?"
            do {
                let resultSet = try database.executeQuery(query, values: [event_id])
                while resultSet.next() {
                    let messageId = resultSet.string(forColumn: "message_id") ?? ""
                    let message_content = resultSet.string(forColumn: "message_content") ?? ""
                    //Don't really need this
                    let eventId = resultSet.string(forColumn: "event_id") ?? ""
                    let userId = resultSet.string(forColumn: "user_id") ?? ""
                    let timestampString = resultSet.string(forColumn: "timestamp") ?? ""
                    let displayName = resultSet.string(forColumn: "display_name") ?? ""
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    //Since the first few messages have no display name
                    if displayName != "" {
                        if let timestamp = formatter.date(from: timestampString) {
                            let sender = Sender(photoURL: "OU", senderId: userId, displayName: displayName)
                            let message = Message(sender: sender, messageId: messageId, sentDate: timestamp, kind: .text(message_content))
                            messages.append(message)
                        }
                    }
                }
            } catch {
                print("Error executing query: \(error.localizedDescription)")
            }
            database.close()
        } else {
            print("Error opening database")
        }

        //print("Event Id \(event_id)")
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate  = self
        
        messagesCollectionView.reloadData()
        
        startTimer()
        
    }
    
    @objc private func navigationItemTitleTapped() {
        // Group chat details
        performSegue(withIdentifier: "groupInfoSegue", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let groupInfoVC = segue.destination as? GroupInfoViewController {
            groupInfoVC.groupName = event_title
            groupInfoVC.event_id = event_id
            let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
            let database = FMDatabase(path: databasePath)
            if database.open() {
                let eventQuery = "SELECT * FROM Events WHERE id = ?"
                do {
                    let resultSet = try database.executeQuery(eventQuery, values: [event_id])
                    while resultSet.next() {
                        let address = resultSet.string(forColumn: "location") ?? "NA"
                        let date = resultSet.string(forColumn: "date") ?? "NA"
                        let time = resultSet.string(forColumn: "time") ?? "NA"
                        groupInfoVC.address = address
                        groupInfoVC.dateTime = date + " at " + time
                        groupInfoVC.desc = resultSet.string(forColumn: "description") ?? ""
                    }
                }
                catch {
                    
                }
                database.close()
            }
            else {
                print("Couldn't access DB")
            }
        }
    }
    
    func currentSender() -> MessageKit.SenderType {
        return currentChatter
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessageKit.MessagesCollectionView) -> MessageKit.MessageType {
        return messages[indexPath.section]
    }
    
    func numberOfSections(in messagesCollectionView: MessageKit.MessagesCollectionView) -> Int {
        messages.count
    }
    
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        guard !text.replacingOccurrences(of: " ", with: "").isEmpty else {
            return
        }
        //not empty
        
        let newMessage = Message(sender: currentChatter, messageId: UUID().uuidString, sentDate: Date(), kind: .text(text))
        guard case let .text(text) = newMessage.kind else {
            return
        }
        guard let messageContent = text as? NSString else {
            return
        }
        messages.append(newMessage)
        messagesCollectionView.reloadData()
        
        messageInputBar.inputTextView.text = ""
        
        
        //Adding to Messages
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        if database.open() {
            let insertSQL = "INSERT INTO Messages (message_content, event_id, user_id, timestamp, display_name) VALUES (?, ?, ?, ?, ?)"
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let timestampString = formatter.string(from: newMessage.sentDate)
            print(messageContent)
            let values = [messageContent, event_id, currentChatter.senderId, timestampString, currentChatter.displayName] as [Any]
            let success = database.executeUpdate(insertSQL, withArgumentsIn: values)
            if !success {
                print("Error inserting message into Messages table")
            }
            database.close()
        } else {
            print("Error adding new message into database")
        }
    }
    
    func messageTopLabelAttributedText(for message: MessageKit.MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = message.sender.displayName
        return NSAttributedString(string: name, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption1)])
    }


    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        if message.sender.senderId == currentChatter.senderId {
            return 0 
        }
        return 15
        }
    
    @objc private func backButtonTapped() {
        dismiss(animated: true, completion: nil)
    }
    
    //Refreshing every second
    var timer: Timer?

    func startTimer() {
        //Every second
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { [weak self] timer in
            self?.fetchNewMessages()
        })
    }

    func stopTimer() {
        timer?.invalidate()
        timer = nil
    }

    func fetchNewMessages() {
        messages.removeAll()
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        if database.open() {
            let query = "SELECT * FROM Messages WHERE event_id = ?"
            do {
                let resultSet = try database.executeQuery(query, values: [event_id])
                while resultSet.next() {
                    let messageId = resultSet.string(forColumn: "message_id") ?? ""
                    let message_content = resultSet.string(forColumn: "message_content") ?? ""
                    //Don't really need this
                    let eventId = resultSet.string(forColumn: "event_id") ?? ""
                    let userId = resultSet.string(forColumn: "user_id") ?? ""
                    let timestampString = resultSet.string(forColumn: "timestamp") ?? ""
                    let displayName = resultSet.string(forColumn: "display_name") ?? ""
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    //Since the first few messages have no display name
                    if displayName != "" {
                        if let timestamp = formatter.date(from: timestampString) {
                            let sender = Sender(photoURL: "OU", senderId: userId, displayName: displayName)
                            let message = Message(sender: sender, messageId: messageId, sentDate: timestamp, kind: .text(message_content))
                            messages.append(message)
                        }
                    }
                }
            } catch {
                print("Error executing query: \(error.localizedDescription)")
            }
            database.close()
        } else {
            print("Error opening database")
        }
        
        messagesCollectionView.reloadData()
    }
}


