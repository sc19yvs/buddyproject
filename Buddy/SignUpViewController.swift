//
//  SignUpViewController.swift
//  Buddy
//
//  Created by Yash salgaonkar on 17/02/23.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet var firstNameText: UITextField!
    
    @IBOutlet var usernameText: UITextField!
    
    @IBOutlet var passwordText: UITextField!
    
    @IBOutlet var confirmPasswordText: UITextField!
    
    @IBOutlet var birthDate: UIDatePicker!
    
    @IBOutlet var emailText: UITextField!
    
    @IBOutlet var courseText: UITextField!
    
    @IBOutlet var passwordErrorLabel: UILabel!
    
    @IBOutlet var emailErrorLabel: UILabel!
    
    @IBOutlet var inputErrorLabel: UILabel!
    
    @IBAction func confirmPasswordDidChange(_ sender: UITextField) {
        let password = passwordText.text ?? ""
            let confirmPassword = sender.text ?? ""
            
            if password != confirmPassword {
                sender.textColor = .red
                passwordErrorLabel.isHidden = false
            } else {
                sender.textColor = .black
                passwordErrorLabel.isHidden = true
            }
    }
    
    @IBAction func confirmEmail(_ sender: UITextField) {
        if sender.text?.contains("@leeds.ac.uk") == true {
            //valid
            sender.textColor = .black
            emailErrorLabel.isHidden = true
        } else {
            //Invalid
            sender.textColor = .red
            emailErrorLabel.isHidden = false
        }
    }
    
    
    //list of selected interests to be stored
    var selectedInterests: [String] = []
    
    @IBAction func footballTapped(_ sender: UIButton) {
        if selectedInterests.contains("1") {
            if let index = selectedInterests.firstIndex(of: "1") {
                selectedInterests.remove(at: index)
            }
            sender.backgroundColor = nil
        } else {
            selectedInterests.append("1")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func walksTapped(_ sender: UIButton) {
        if selectedInterests.contains("2") {
            if let index = selectedInterests.firstIndex(of: "2") {
                selectedInterests.remove(at: index)
            }
            sender.backgroundColor = nil
        } else {
            selectedInterests.append("2")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func barsTapped(_ sender: UIButton) {
        if selectedInterests.contains("4") {
            if let index = selectedInterests.firstIndex(of: "4") {
                selectedInterests.remove(at: index)
            }
            sender.backgroundColor = nil
        } else {
            selectedInterests.append("4")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func clubbingTapped(_ sender: UIButton) {
        if selectedInterests.contains("5") {
            if let index = selectedInterests.firstIndex(of: "5") {
                selectedInterests.remove(at: index)
            }
            sender.backgroundColor = nil
        } else {
            selectedInterests.append("5")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func pubsTapped(_ sender: UIButton) {
        if selectedInterests.contains("3") {
            if let index = selectedInterests.firstIndex(of: "3") {
                selectedInterests.remove(at: index)
            }
            sender.backgroundColor = nil
        } else {
            selectedInterests.append("3")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    
    @IBAction func musicTapped(_ sender: UIButton) {
        if selectedInterests.contains("6") {
            if let index = selectedInterests.firstIndex(of: "6") {
                selectedInterests.remove(at: index)
            }
            sender.backgroundColor = nil
        } else {
            selectedInterests.append("6")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    @IBAction func danceTapped(_ sender: UIButton) {
        if selectedInterests.contains("7") {
            if let index = selectedInterests.firstIndex(of: "7") {
                selectedInterests.remove(at: index)
            }
            sender.backgroundColor = nil
        } else {
            selectedInterests.append("7")
            sender.backgroundColor = UIColor.systemOrange
        }
    }
    
    
    
    
    
    
    
    
    
    //YS This is temporary, needs to replaced by a selection grid
    @IBOutlet var interestsText: UITextField!
    
    
    @IBAction func signUpTapped(_ sender: Any) {
        if(emailErrorLabel.isHidden == false || passwordErrorLabel.isHidden == false){
            inputErrorLabel.isHidden = false
        }
        else{
            inputErrorLabel.isHidden = true
            let firstName = firstNameText.text ?? ""
            let lastName =  ""
            let email = emailText.text ?? ""
            let username = usernameText.text ?? ""
            let password = passwordText.text ?? ""
            let course = courseText.text ?? ""
            let profilePictureData = Data()
            let description = "I'm an outgoing person"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy" // choose your desired format
            let birthDateString = dateFormatter.string(from: birthDate.date)
            
            
            let newUser = User(first_name: firstName, last_name: lastName,username: username, password: password, date_of_birth: birthDateString, description: description,course: course, buddy_enabled: "yes", location_enabled: "no", profilePicture: Data())
            
            let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
            let database = FMDatabase(path: databasePath)
            
            guard database.open() else {
                print("Unable to open database!")
                exit(1)
            }
            
            //Entering user in database
            let interestsIds = [1, 4, 2] // assume Football, Pub, and Clubbing
            let insertUserQuery = "INSERT INTO User (username, password, first_name, last_name, date_of_birth, description, course, profile_picture) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
            //let userId = database.lastInsertRowId
            if !database.executeUpdate(insertUserQuery, withArgumentsIn: [username, password, firstName, lastName, birthDateString, description, course, profilePictureData]) {
                print("Failed to insert user:", database.lastErrorMessage())
            }
            
            // insert user interests into the relationship table
            let userId = database.lastInsertRowId
            
            let insertUserInterestQuery = "INSERT INTO UserInterests (user_id, interest_id) VALUES (?, ?)"
            for interestId in selectedInterests {
                if !database.executeUpdate(insertUserInterestQuery, withArgumentsIn: [userId, interestId]) {
                    print("Failed to insert user interest:", database.lastErrorMessage())
                }
            }
            
            // close database
            database.close()
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordErrorLabel.isHidden = true
        emailErrorLabel.isHidden = true
        inputErrorLabel.isHidden = true
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
