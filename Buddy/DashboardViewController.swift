//
//  DashboardViewController.swift
//  Buddy
//
//  Created by Yash salgaonkar on 14/02/23.
//

import UIKit


class DashboardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DashboardTableViewCellDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardTableViewCell", for: indexPath) as! DashboardTableViewCell
        cell.delegate = self
        cell.tabButton.setTitle(titleArray[indexPath.row], for: .normal)
        cell.tabButton.setBackgroundImage(UIImage(named:"bck.jpeg"), for: .normal)

        return cell
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func didTapTabButton(buttonTitle: String) {
        if buttonTitle == "OPEN EVENTS" {
                performSegue(withIdentifier: "openInvitesSegue", sender: nil)
            }
    }
    
    var titleArray: [String] = ["OPEN EVENTS", "TIPS TO KEEP SAFE", "A QUICK GUIDE"]
    

    @IBOutlet var dashboardTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib(nibName: "DashboardTableViewCell", bundle: nil)
        
        dashboardTableView.register(nib, forCellReuseIdentifier: "DashboardTableViewCell")
        
        dashboardTableView.delegate = self
        dashboardTableView.dataSource = self
        dashboardTableView.reloadData()
        
        
    }
    
    
    
}
