//
//  UserTableViewCell.swift
//  Buddy
//
//  Created by Yash salgaonkar on 03/04/23.
//

import UIKit

protocol UserTableViewCellDelegate: AnyObject {

}

class UserTableViewCell: UITableViewCell {

    weak var delegate: UserTableViewCellDelegate?
    
    @IBOutlet var profileImageView: UIImageView!
    
    @IBOutlet var nameLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profileImageView.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
