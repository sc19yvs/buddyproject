//
//  SettingsTableViewCell.swift
//  Buddy
//
//  Created by Yash salgaonkar on 31/03/23.
//

import UIKit

protocol SettingsTableViewCellDelegate: AnyObject {
    func toggleSwitched(_ sender: Any)
}


class SettingsTableViewCell: UITableViewCell {

    weak var delegate: SettingsTableViewCellDelegate?
    
    @IBOutlet var settingNameLabel: UILabel!
    

    
    @IBAction func toggleSwitched(_ sender: Any) {
        delegate?.toggleSwitched(sender)
    }
    
    
    @IBOutlet var toggleSwitch: UISwitch!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
