//
//  SettingsViewController.swift
//  Buddy
//
//  Created by Yash salgaonkar on 17/02/23.
//

import UIKit

var loc = "yes"
var bud = "no"

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SettingsTableViewCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return generalArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell", for: indexPath) as! SettingsTableViewCell
        cell.settingNameLabel.text = generalArray[indexPath.row]
        cell.delegate = self
        
        
        switch indexPath.row {
        case 0: // Buddy Ups
            cell.toggleSwitch.isOn = (current_user?.buddy_enabled == "yes")
        case 1: // Location
            cell.toggleSwitch.isOn = (current_user?.location_enabled == "yes")
        default:
            break
        }
        return cell
    }
    
    var generalArray = ["Buddy Ups", "Location"]
    
    @IBOutlet var generalTableView: UITableView!
    
    
    
    
    @IBAction func logOutTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Log out", message: "Are you sure you want to log out?", preferredStyle: .alert)
                
        let confirmAction = UIAlertAction(title: "Yes", style: .default) { (_) in
            
            //Clear info
            UserDefaults.standard.removeObject(forKey: "loggedInUser")
            
            //Clear user
            current_user = nil
            
            // Perform logout action here
            self.performSegue(withIdentifier: "backToLoginSegue", sender: nil)
            
            
        }
        alertController.addAction(confirmAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generalTableView.dataSource = self
        generalTableView.delegate = self
        
        let nib = UINib(nibName: "SettingsTableViewCell", bundle: nil)
        
        generalTableView.register(nib, forCellReuseIdentifier: "SettingsTableViewCell")
        
        
    }
    
    func toggleSwitched(_ sender: Any) {
        guard let toggleSwitch = (sender as? UISwitch) else {
            return
        }
        
        guard let cell = toggleSwitch.superview?.superview as? SettingsTableViewCell else {
            return
        }
        
        let indexPath = generalTableView.indexPath(for: cell)
        
        switch indexPath?.row {
        case 0:
            current_user?.buddy_enabled = toggleSwitch.isOn ? "yes" : "no"
        case 1:
            current_user?.location_enabled = toggleSwitch.isOn ? "yes" : "no"
        default:
            break
        }
        
        //Now update database
        print("Buddy \(current_user!.buddy_enabled)")
        print("Location \(current_user!.location_enabled)")
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        
        if database.open() {
            let query = "UPDATE User SET buddy_enabled = ? AND location_enabled = ? WHERE username = ?"
            do {
                //we know these values are never nil
                print("Updating database with values: buddy_enabled=\(current_user!.buddy_enabled), location_enabled=\(current_user!.location_enabled), username=\(current_user!.username)")
                try database.executeUpdate(query, values: [current_user!.buddy_enabled, current_user!.location_enabled, current_user!.username])
                print("Updated database")
            } catch {
                print("Error updating value in database: \(error.localizedDescription)")
            }
            database.close()
        } else {
            print("Error opening database connection")
        }
        
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    
    func tableView(_ tableView: UITableView, widthForRowAt indexPath: IndexPath) -> CGFloat {
        return 320
    }
    

}
