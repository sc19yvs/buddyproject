//
//  InvitesViewController.swift
//  Buddy
//
//  Created by Yash salgaonkar on 17/02/23.
//

import UIKit

class InvitesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, InvitesTableViewCellDelegate {

    func didTapAcceptButton(eventId: String) {
            updateEventInvitees(eventId: eventId, response: "1")
        }
        
    func didTapDeclineButton(eventId: String) {
            updateEventInvitees(eventId: eventId, response: "2")
        }
    
    
    var event_ids: [String] = []
    var events: [Event] = []
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setting up access to database
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        if !database.open() {
            print("Unable to open database")
            return
        }
        do {
            let results = try database.executeQuery("SELECT * FROM EventInvitees WHERE user_id = ? AND response = ?", values: [current_user?.username, "0"])
            while results.next() {
                guard let event_id = results.string(forColumn: "event_id")
                      
                else {
                    continue
                }
                
                event_ids.append(event_id)
            }

        } catch {
            print("Failed to fetch event invitee data: \(error.localizedDescription)")
        }
        print("Invite count:\(event_ids.count)" )
        //Now accessing Events Table
        do {
            let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
            let database = FMDatabase(path: databasePath)
            if !database.open() {
                print("Unable to open database")
                return
            }
            let results = try database.executeQuery("SELECT * FROM Events WHERE id IN (\(event_ids.map {"'\($0)'"}.joined(separator: ",")))", values: nil)
            
            while results.next() {
                //Accessing column info
                guard let event_id = results.string(forColumn: "id"),
                      let location = results.string(forColumn: "location"),
                      let date = results.string(forColumn: "date"),
                      let time = results.string(forColumn: "time"),
                      //let invitees = results.string(forColumn: "invitees"),
                      let creator_id = results.string(forColumn: "creator_id"),
                      let title = results.string(forColumn: "title"),
                      let description = results.string(forColumn: "description")
                      
                        
                      
                else {
                    continue
                }
                let event = Event(id:event_id, location: location, date: date, time: time, creator_id: creator_id, title: title, description: description )
                events.append(event)
            }
            tableView.delegate = self
            tableView.dataSource = self
            tableView.reloadData()
        } catch {
            print("Failed to fetch event data: \(error.localizedDescription)")
        }
        
        database.close()
        //End of database section
        
        //Setting up vertical stack here
        //Referene YT video
        let nib = UINib(nibName: "InvitesTableViewCell", bundle: nil)
        
        tableView.register(nib, forCellReuseIdentifier: "InvitesTableViewCell")
        
        //tableView.delegate = self
        //tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if events.isEmpty {
            // Display message when there are no events
            tableView.setEmptyMessage("You have no Invites at the moment. Go on and create an Event on the Create page!")
            return 0
        } else {
            // Display number of events
            tableView.restore()
            return events.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 270
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InvitesTableViewCell", for: indexPath) as! InvitesTableViewCell
        let event = events[indexPath.row]
        cell.titleLabel?.text = "\(event.title)"
        cell.addressLabel.text = "\(event.location)"
        cell.descriptionLabel.text = "\(event.description)"
        cell.dateTimeLabel.text = "\(event.date) At \(event.time)"
        cell.eventId = event.id
        cell.delegate = self
        return cell
    }
    
    //To update status after user taps
    private func updateEventInvitees(eventId: String, response: String) {
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        guard database.open() else {
            print("Unable to open database")
            return
        }
        
        do {
            try database.executeUpdate("UPDATE EventInvitees SET response = ? WHERE user_id = ? AND event_id = ?", values: [response, current_user?.username, eventId])
            event_ids.removeAll { $0 == eventId }
            if let index = events.firstIndex(where: { $0.id == eventId }) {
                events.remove(at: index)
            }
            tableView.reloadData()
        } catch {
            print("Failed to update event invitee data: \(error.localizedDescription)")
        }
        
        database.close()
        
    }
    
    
    

    

}

extension UITableView {
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.systemFont(ofSize: 20)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}







