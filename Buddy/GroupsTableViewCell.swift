//
//  GroupsTableViewCell.swift
//  Buddy
//
//  Created by Yash salgaonkar on 08/03/23.
//

import UIKit

protocol GroupsTableViewCellDelegate: AnyObject {
    
}

class GroupsTableViewCell: UITableViewCell {

    weak var delegate: GroupsTableViewCellDelegate?
    
    @IBOutlet var groupLabel: UILabel!
        
    @IBOutlet var numberLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
