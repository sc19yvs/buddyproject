//
//  ProfileViewController.swift
//  Buddy
//
//  Created by Yash salgaonkar on 30/03/23.
//

import UIKit

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    
    @IBOutlet var nameLabel: UILabel!
    
    
    @IBOutlet var courseLabel: UILabel!
    
    @IBOutlet var aboutTextView: UITextView!
    
    
    @IBOutlet var interestsTextView: UITextView!
    
    @IBOutlet var profileImageView: UIImageView!
    
    var selectedImage: UIImage?
    
    
    @IBOutlet var editDescription: UIButton!
    
    @IBAction func editDescriptionTapped(_ sender: Any) {
        if editDescription.currentTitle == "Edit" {
            editDescription.setTitle("Done", for: .normal)
            aboutTextView.isEditable = true
            aboutTextView.becomeFirstResponder()
        } else {
            editDescription.setTitle("Edit", for: .normal)
            aboutTextView.isEditable = false

            //alter database
            let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
            let database = FMDatabase(path: databasePath)
            
            //Edit database
            if database.open() {

                let updateQuery = "UPDATE User SET description = ? WHERE username = ?"
                let success = database.executeUpdate(updateQuery, withArgumentsIn: [aboutTextView.text, current_user!.username])
                if !success{
                    print("Failed to update description:(")
                }
                
                database.close()
            } else {
                print("Unable to open database to update description!")
            }

            if database.open() {
                //Fetch updated description into current user
                let query = "SELECT * FROM User WHERE username = ?"
                let result = database.executeQuery(query, withArgumentsIn: [current_user!.username])
                while (result!.next()) {
                    if let resultUsername = result?.string(forColumn: "username"),
                       let resultPassword = result?.string(forColumn: "password"),
                       let resultFirstName = result?.string(forColumn: "first_name"),
                       let resultLastName = result?.string(forColumn: "last_name"),
                       let resultDOB = result?.string(forColumn: "date_of_birth"),
                       let resultDescription = result?.string(forColumn: "description"),
                       let resultCourse = result?.string(forColumn: "course"),
                       let buddy_enabled = result?.string(forColumn: "buddy_enabled"),
                       let location_enabled = result?.string(forColumn: "location_enabled"){
                           current_user = User(first_name: resultFirstName, last_name: resultLastName, username: resultUsername, password: resultPassword, date_of_birth: resultDOB, description: resultDescription, course: resultCourse, buddy_enabled: buddy_enabled, location_enabled: location_enabled, profilePicture: result?.data(forColumn: "profile_picture") ?? Data())
                    }
                }
                database.close()
                
            } else {
                print("Unable to update description in current_user! ")
            }
        }
    }
    
    
    @IBAction func editImageTapped(_ sender: UIButton) {
        let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.allowsEditing = true
        let actionSheet = UIAlertController(title: "Add a photo", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            } else {
                print("Camera not available")
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Choose From Library", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let editedImage = info[.editedImage] as? UIImage
        let originalImage = info[.originalImage] as? UIImage
        let selectedImage = editedImage ?? originalImage
        self.selectedImage = selectedImage
        //profileImageView.contentMode = .scaleAspectFill
        profileImageView.image = selectedImage
        
        picker.dismiss(animated: true, completion: nil)
        
        //alter database
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        
        //Edit database
        if database.open() {
            if let imageData = selectedImage?.jpegData(compressionQuality: 1.0) {
                let updateQuery = "UPDATE User SET profile_picture = ? WHERE username = ?"
                let success = database.executeUpdate(updateQuery, withArgumentsIn: [imageData, current_user!.username])
                if !success {
                    print("Failed to update profile picture:(")
                }
            }
            database.close()
        } else {
            print("Unable to open database to update profile picture!")
        }
        
        
        
        
        
        if database.open() {
            //Fetch updated profile into current user
            let query = "SELECT * FROM User WHERE username = ?"
            let result = database.executeQuery(query, withArgumentsIn: [current_user!.username])
            while (result!.next()) {
                if let resultUsername = result?.string(forColumn: "username"),
                   let resultPassword = result?.string(forColumn: "password"),
                   let resultFirstName = result?.string(forColumn: "first_name"),
                   let resultLastName = result?.string(forColumn: "last_name"),
                   let resultDOB = result?.string(forColumn: "date_of_birth"),
                   let resultDescription = result?.string(forColumn: "description"),
                   let resultCourse = result?.string(forColumn: "course"),
                   let buddy_enabled = result?.string(forColumn: "buddy_enabled"),
                   let location_enabled = result?.string(forColumn: "location_enabled"){
                       current_user = User(first_name: resultFirstName, last_name: resultLastName, username: resultUsername, password: resultPassword, date_of_birth: resultDOB, description: resultDescription, course: resultCourse, buddy_enabled: buddy_enabled, location_enabled: location_enabled, profilePicture: result?.data(forColumn: "profile_picture") ?? Data())
                }
            }
            database.close()
            
        } else {
            print("Unable to update profile picture in current_user! ")
        }
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        nameLabel.text = current_user?.first_name
        courseLabel.text = "Course: " + current_user!.course
        aboutTextView.text = current_user?.description
        
        
        var interestsText = ""
        
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        
        if database.open(){
            
            //let query = "SELECT tags.tag_name FROM tags JOIN UserTags ON tags.id = UserTags.tag_id WHERE UserTags.username = ?"
            //let result = database.executeQuery(query, withArgumentsIn: [current_user!.username])

            /*
            while result!.next() {
                if let tagName = result!.string(forColumn: "tag_name") {
                    //interestsText = interestsText + tagName + ", "
              }
            }
            */
            
            //set profile picture
            let userQuery = "SELECT * FROM User WHERE username  = ?"
            let userResult = database.executeQuery(userQuery, withArgumentsIn: [current_user!.username])
            
            while userResult!.next(){
                if let imageData = userResult!.data(forColumn: "profile_picture") {
                        let image = UIImage(data: imageData)
                        profileImageView.image = image
                    }
            }
        
            
            database.close()
        }
        else {
            print("Unable to open database to fetch profile picture")
        }
        
        //interestsText.removeLast()
        //interestsText.removeLast()
        //interestsTextView.text = interestsText
        
        
        
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
          nameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
          nameLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 300)
        ])
    }
    



}
