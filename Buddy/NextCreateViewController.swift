//
//  NextCreateViewController.swift
//  Buddy
//
//  Created by Yash salgaonkar on 22/02/23.
//

import UIKit

class CheckboxTableViewCell: UITableViewCell {
    
    weak var delegate: CheckboxTableViewCellDelegate?
    
    @IBOutlet var checkBox: UIButton!
    var isChecked: Bool = false {
            didSet {
                checkBox.isSelected = isChecked
            }
        }
    @IBAction func checkBoxSelected(_ sender: Any) {
        isChecked.toggle()
        delegate?.checkboxTableViewCellDidToggleCheckbox(self) //selecting
    }
    
    
}

//trying selecting with checkbox instead of cell
protocol CheckboxTableViewCellDelegate: AnyObject {
    func checkboxTableViewCellDidToggleCheckbox(_ cell: CheckboxTableViewCell)
    
    
}


class NextCreateViewController: UIViewController, CheckboxTableViewCellDelegate {

    weak var delegate: CreateViewController?
    
    var selectedUsers: [User] = []
    var users: [User] = []

    @IBOutlet var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setting up database access here
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        if !database.open() {
            print("Unable to open database")
            return
        }
        do {
            let results = try database.executeQuery("SELECT * FROM User", values: nil)
            while results.next() {
                guard let firstName = results.string(forColumn: "first_name"),
                      let lastName = results.string(forColumn: "last_name"),
                      let username = results.string(forColumn: "username"),
                      let password = results.string(forColumn: "password"),
                      let date_of_birth = results.string(forColumn: "date_of_birth"),
                      let description = results.string(forColumn: "description"),
                      let course = results.string(forColumn: "course"),
                      let buddyEnabled = results.string(forColumn: "buddy_enabled"),
                      let locationEnabled = results.string(forColumn: "location_enabled")
                      //let profilePicture = results.data(forColumn: "profile_picture") ?? Data()
                else {
                    continue
                }
                let user = User(first_name: firstName, last_name: lastName, username: username, password: password, date_of_birth: date_of_birth, description: description, course: course, buddy_enabled: buddyEnabled, location_enabled: locationEnabled, profilePicture: results.data(forColumn: "profile_picture") ?? Data())
                users.append(user)
            }
            tableView.delegate = self
            tableView.dataSource = self
            tableView.reloadData()
        } catch {
            print("Failed to fetch user data: \(error.localizedDescription)")
        }
        print(users.count)
        database.close()
    }

    @IBAction func addTapped(_ sender: Any) {
        delegate?.nextCreateViewControllerDidSelectUsers(selectedUsers: selectedUsers)
                dismiss(animated: true, completion: nil)
    }

    
    
    
    
    func checkboxTableViewCellDidToggleCheckbox(_ cell: CheckboxTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        let selectedUser = users[indexPath.row]
        if cell.isChecked {
            selectedUsers.append(selectedUser)
        } else {
            if let index = selectedUsers.firstIndex(of: selectedUser) {
                selectedUsers.remove(at: index)
            }
        }
        print(selectedUsers.count)
    }

}

extension NextCreateViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedUser = users[indexPath.row]
        selectedUsers.append(selectedUser)
        //tableView.reloadData() //Testing
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let deselectedUser = users[indexPath.row]
        if let index = selectedUsers.firstIndex(of: deselectedUser) {
            selectedUsers.remove(at: index)
        }
        //tableView.reloadData() //Testing
    }

}

extension NextCreateViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "user", for: indexPath) as! CheckboxTableViewCell
        let user = users[indexPath.row]
        cell.textLabel?.text = "\(user.first_name) \(user.last_name)"
        cell.delegate = self
        return cell
    }

}

