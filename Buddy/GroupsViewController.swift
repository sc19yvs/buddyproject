//
//  GroupsViewController.swift
//  Buddy
//
//  Created by Yash salgaonkar on 17/02/23.
//

import UIKit

class GroupsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, GroupsTableViewCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupsTableViewCell", for: indexPath) as! GroupsTableViewCell
        let event = events[indexPath.row]
        cell.groupLabel.text = event.title
        //cell.numberLabel.text = "x members"
        cell.delegate = self
        return cell
    }
    

    //Similar to invites page
    var event_ids: [String] = []
    var events: [Event] = []
    
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setting up access to database
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        if !database.open() {
            print("Unable to open database")
            return
        }
        do {
            let results = try database.executeQuery("SELECT * FROM EventInvitees WHERE user_id = ? AND response = ?", values: [current_user?.username, "1"])
            while results.next() {
                guard let event_id = results.string(forColumn: "event_id")
                      
                else {
                    continue
                }
                
                event_ids.append(event_id)
            }

        } catch {
            print("Failed to fetch event invitee data: \(error.localizedDescription)")
        }
        
        
        //Storing in Events
        do {
            let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
            let database = FMDatabase(path: databasePath)
            if !database.open() {
                print("Unable to open database")
                return
            }
            let results = try database.executeQuery("SELECT * FROM Events WHERE id IN (\(event_ids.map {"'\($0)'"}.joined(separator: ",")))", values: nil)
            
            while results.next() {
                //Accessing column info
                guard let event_id = results.string(forColumn: "id"),
                      let location = results.string(forColumn: "location"),
                      let date = results.string(forColumn: "date"),
                      let time = results.string(forColumn: "time"),
                      //let invitees = results.string(forColumn: "invitees"),
                      let creator_id = results.string(forColumn: "creator_id"),
                      let title = results.string(forColumn: "title"),
                      let description = results.string(forColumn: "description")
                      
                        
                      
                else {
                    continue
                }
                let event = Event(id:event_id, location: location, date: date, time: time, creator_id: creator_id, title: title, description: description )
                events.append(event)
            }
            tableView.delegate = self
            tableView.dataSource = self
            tableView.reloadData()
        } catch {
            print("Failed to fetch event data: \(error.localizedDescription)")
        }
        
        database.close()
        
        
        let nib = UINib(nibName: "GroupsTableViewCell", bundle: nil)
        
        tableView.register(nib, forCellReuseIdentifier: "GroupsTableViewCell")
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 67
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "groupChatSegue", sender: events[indexPath.row])
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "groupChatSegue", let event = sender as? Event {
            let navigationController = segue.destination as! UINavigationController
            let destinationVC = navigationController.topViewController as! GroupChatViewController
            destinationVC.event_id = event.id
            destinationVC.event_title = event.title
        }
    }

}
