//
//  InvitesTableViewCell.swift
//  Buddy
//
//  Created by Yash salgaonkar on 06/03/23.
//

import UIKit



protocol InvitesTableViewCellDelegate: AnyObject {
    func didTapAcceptButton(eventId: String)
    func didTapDeclineButton(eventId: String)
}

class InvitesTableViewCell: UITableViewCell {
    weak var delegate: InvitesTableViewCellDelegate?

    var eventId: String?
    
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var dateTimeLabel: UILabel!
    
    
    @IBOutlet var addressLabel: UILabel!
    
    @IBOutlet var descriptionLabel: UILabel!
    
    @IBAction func acceptTapped(_ sender: Any) {
        if let eeventId = eventId {
            delegate?.didTapAcceptButton(eventId: eeventId)
            //print("tapped accept, eventId: \(eeventId)")
        } else {
            print("eventId is nil")
        }
        //Add the tags as interests if they don't exist already
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        
        if database.open() {
            // For every tag in event's tags, check if the tag exists for user as well, if not, add it
            var tagsArray: [String] = []
            let eventTagQuery = "SELECT * FROM EventTags WHERE event_id = ?"
            let eventResults = database.executeQuery(eventTagQuery, withArgumentsIn: [eventId])
            if eventResults != nil {
                while eventResults!.next() {
                    tagsArray.append(eventResults!.string(forColumn: "tag_id")!)
                }
            }
            
            for tagId in tagsArray {
                let userTagQuery = "INSERT INTO UserTags (username, tag_id) SELECT ?, ? WHERE NOT EXISTS (SELECT 1 FROM UserTags WHERE username = ? AND tag_id = ?)"
                let success = database.executeUpdate(userTagQuery, withArgumentsIn: [current_user?.username, tagId, current_user?.username, tagId])
                if !success {
                   print("Couldn't add interest to user database")
                }
            }
            
            database.close()
        }
        
        
    }
    
    @IBAction func declineTapped(_ sender: Any) {
        if let eeventId = eventId {
            delegate?.didTapDeclineButton(eventId: eeventId)
            //print("tapped decline, eventId: \(eeventId)")
        } else {
            print("eventId is nil")
        }
        
        //Remove the tags from interests if they exist
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        
        if database.open() {
            // For every tag in event's tags, check if the tag exists for user as well, if not, add it
            var tagsArray: [String] = []
            let eventTagQuery = "SELECT * FROM EventTags WHERE event_id = ?"
            let eventResults = database.executeQuery(eventTagQuery, withArgumentsIn: [eventId])
            if eventResults != nil {
                while eventResults!.next() {
                    tagsArray.append(eventResults!.string(forColumn: "tag_id")!)
                }
            }
            
            for tagId in tagsArray {
                let userTagQuery = "DELETE FROM UserTags WHERE username = ? AND tag_id = ?"
                let success = database.executeUpdate(userTagQuery, withArgumentsIn: [current_user?.username, tagId])
                if !success {
                   print("Couldn't remove interest from user database")
                }
            }
            
            database.close()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
