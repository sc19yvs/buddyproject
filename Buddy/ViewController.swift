//
//  ViewController.swift
//  Buddy
//
//  Created by Yash salgaonkar on 11/02/23.
//

import UIKit
import SQLite3


var current_user: User? //current user after login, global variable

class ViewController: UIViewController {
    
    @IBOutlet var incorrectText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        incorrectText.isHidden = true
        current_user = nil  //Setting it to nothing when screen is loaded, might not be the best approach YS
        
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }

        /*
        //Adding image to test
        if let image = UIImage(named: "profilePic.jpeg"){
            if database.open(){
                if let imageData = image.jpegData(compressionQuality: 1.0) {
                    let updateQuery = "UPDATE User SET profile_picture = ?"
                    let success = database.executeUpdate(updateQuery, withArgumentsIn: [imageData])
                    if !success {
                        print("Error updating profile picture")
                    }
                }
                database.close()
            }
            else{
                print("Unable to update profile pictures")
            }
            
        }
        else{
            print("Image was nil")
        }
        //end of image bit
        */
        
        //var weeklyEvent = appDelegate.createWeeklyEvent()
        
        /*
        let content = UNMutableNotificationContent()
        content.title = weeklyEvent.title
        content.body = weeklyEvent.description
        content.sound = .default
        
        let request = UNNotificationRequest(identifier: "MondayNotification", content: content, trigger: nil)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
         */
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if checkAuthStatus() {
            //already logged in
            performSegue(withIdentifier: "dashboardSegue", sender: Any?.self)
        } else {
            //do nothing club
        }
    }
   
    
    @IBOutlet var usernameText: UITextField!
    
    @IBOutlet var passwordText: UITextField!
    
    
    @IBAction func loginTapped(_ sender: UIButton) {
        if let username = usernameText.text?.trimmingCharacters(in: .whitespaces),
            let password = passwordText.text?.trimmingCharacters(in: .whitespaces) {
            if authenticated(username: username, password: password) == true
            {

                
                //Successfully logged in, so storing token
                let token = "userIsInBuddy" // replace with your authentication logic
                let userDict = ["username": username, "token": token]
                UserDefaults.standard.set(userDict, forKey: "loggedInUser")
                
                
                incorrectText.isHidden = true
                performSegue(withIdentifier: "dashboardSegue", sender: Any?.self)
            }
            else{
                usernameText.text = ""
                passwordText.text = ""
                incorrectText.isHidden = false
            }
        }
        
    }
    
    func authenticated(username: String, password: String) -> Bool {
        let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
        let database = FMDatabase(path: databasePath)
        if database.open() {
            let query = "SELECT * FROM user WHERE username = ? AND password = ?"
            let result = database.executeQuery(query, withArgumentsIn: [username, password])
            if result != nil && result!.next()  {
  
                if let resultUsername = result?.string(forColumn: "username"),
                   let resultPassword = result?.string(forColumn: "password"),
                   let resultFirstName = result?.string(forColumn: "first_name"),
                   let resultLastName = result?.string(forColumn: "last_name"),
                   let resultDOB = result?.string(forColumn: "date_of_birth"),
                   let resultDescription = result?.string(forColumn: "description"),
                   let resultCourse = result?.string(forColumn: "course"),
                   let buddy_enabled = result?.string(forColumn: "buddy_enabled"),
                   let location_enabled = result?.string(forColumn: "location_enabled"){
                       current_user = User(first_name: resultFirstName, last_name: resultLastName, username: resultUsername, password: resultPassword, date_of_birth: resultDOB, description: resultDescription, course: resultCourse, buddy_enabled: buddy_enabled, location_enabled: location_enabled, profilePicture: result?.data(forColumn: "profile_picture") ?? Data())
                }
                return true
            }
            database.close()
        }
        return false
    }
    
    func checkAuthStatus() -> Bool {
        if let userDict = UserDefaults.standard.dictionary(forKey: "loggedInUser") {
            let username = userDict["username"] as! String
            let token = userDict["token"] as! String
            
            //Token verification
            if token == "userIsInBuddy" {
                let databasePath = "/Users/yashsalgaonkar/Desktop/Buddy/database.sqlite"
                let database = FMDatabase(path: databasePath)
                if database.open() {
                    let query = "SELECT * FROM user WHERE username = ?"
                    let result = database.executeQuery(query, withArgumentsIn: [username])
                    if result != nil && result!.next()  {
                        
                        if let resultUsername = result?.string(forColumn: "username"),
                           let resultPassword = result?.string(forColumn: "password"),
                           let resultFirstName = result?.string(forColumn: "first_name"),
                           let resultLastName = result?.string(forColumn: "last_name"),
                           let resultDOB = result?.string(forColumn: "date_of_birth"),
                           let resultDescription = result?.string(forColumn: "description"),
                           let resultCourse = result?.string(forColumn: "course"),
                           let buddy_enabled = result?.string(forColumn: "buddy_enabled"),
                           let location_enabled = result?.string(forColumn: "location_enabled") {
                            current_user = User(first_name: resultFirstName, last_name: resultLastName, username: resultUsername, password: resultPassword, date_of_birth: resultDOB, description: resultDescription, course: resultCourse, buddy_enabled: buddy_enabled, location_enabled: location_enabled, profilePicture: result?.data(forColumn: "profile_picture") ?? Data())
                        }
                        database.close()
                        return true
                    }
                    //database.close()
                }
            }
        }
        return false
    }

}



//Code to create tables
/*
 database.open()
 let userTable = """
 CREATE TABLE User (
     id INTEGER PRIMARY KEY AUTOINCREMENT,
     username TEXT UNIQUE,
     password TEXT,
     first_name TEXT,
     last_name TEXT,
     date_of_birth TEXT,
     description TEXT,
     course TEXT,
     profile_picture BLOB
 );
 """

 let interestTable = """
 CREATE TABLE Interests (
     id INTEGER PRIMARY KEY AUTOINCREMENT,
     interest TEXT UNIQUE
 );
 """

 let userInterestTable = """
 CREATE TABLE UserInterests (
     user_id INTEGER,
     interest_id INTEGER,
     FOREIGN KEY (user_id) REFERENCES User (id) ON DELETE CASCADE,
     FOREIGN KEY (interest_id) REFERENCES Interests (id) ON DELETE CASCADE,
     PRIMARY KEY (user_id, interest_id)
 );
 """

 do {
     try database.executeUpdate(userTable, values: nil)
     try database.executeUpdate(interestTable, values: nil)
     try database.executeUpdate(userInterestTable, values: nil)
     print("Tables created successfully")
 } catch {
     print("Error creating tables: \(error.localizedDescription)")
 }
 database.close()
 */

/*
// populate Interests table with interests
let interests = ["Football", "Walks", "Pub", "Bars", "Clubbing", "Music", "Dance"]
let insertInterestQuery = "INSERT INTO Interests (interest) VALUES (?)"
for interest in interests {
    if !database.executeUpdate(insertInterestQuery, withArgumentsIn: [interest]) {
        print("Failed to insert interest:", database.lastErrorMessage())
    }
}
 */

//Events table
/*
 let createEventsTableQuery = "CREATE TABLE Events (id INTEGER PRIMARY KEY AUTOINCREMENT, location TEXT, date TEXT, time TEXT, invitees TEXT, attendees TEXT, creator_id INTEGER, FOREIGN KEY (creator_id) REFERENCES User(id))"
 if !database.executeUpdate(createEventsTableQuery, withArgumentsIn: []) {
     print("Failed to create Events table:", database.lastErrorMessage())
 }
 */
/*
 let createEventInviteesQuery = """
 CREATE TABLE IF NOT EXISTS EventInvitees (
   event_id INTEGER,
   user_id INTEGER,
   response INTEGER,
   FOREIGN KEY (event_id) REFERENCES Events(id),
   FOREIGN KEY (user_id) REFERENCES User(id)
 );
 """

 do {
     try database.executeUpdate(createEventInviteesQuery, values: nil)
 } catch {
     print("Error creating EventInvitees table: \(error.localizedDescription)")
 }
 */
/*
 let createEventsTableQuery = """
 CREATE TABLE IF NOT EXISTS Events (
     id INTEGER PRIMARY KEY AUTOINCREMENT,
     title TEXT,
     description TEXT,
     location TEXT,
     date TEXT,
     time TEXT,
     creator_id INTEGER,
     FOREIGN KEY (creator_id) REFERENCES User(id)
 )
 """
 */
/*
 let addTitleColumnQuery = "ALTER TABLE Events ADD COLUMN title TEXT"
     let addDescriptionColumnQuery = "ALTER TABLE Events ADD COLUMN description TEXT"
     let successTitle = database.executeUpdate(addTitleColumnQuery, withArgumentsIn: [])
     let successDescription = database.executeUpdate(addDescriptionColumnQuery, withArgumentsIn: [])
     
     if successTitle && successDescription {
         print("Columns added successfully!")
     } else {
         print("Failed to add columns: \(database.lastErrorMessage())")
     }
 */
